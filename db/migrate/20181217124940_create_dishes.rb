class CreateDishes < ActiveRecord::Migration[5.2]
  def change
    create_table :dishes do |t|
      t.string :title
      t.text :description
      t.decimal :price
      t.references :restaurant, foreign_key: true

      t.timestamps
    end
  end
end
