Rails.application.routes.draw do
    
  root 'restaurants#index'
  ActiveAdmin.routes(self)
  devise_for :users
  resources :restaurants
  resources :dishes
end
