class DishController < ApplicationController
  def show
  	@dish = Dish.find(params[:id])
  end

  private

  def dishes_params
  	params.require(:dish).permit(:title, :description, :price, :picture)
  end
end
