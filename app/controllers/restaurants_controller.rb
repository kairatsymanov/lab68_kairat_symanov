class RestaurantsController < ApplicationController
  
  def index
  	@restaurants = Restaurant.all 
  end

  def show
  	@restaurant = Restaurant.find(params[:id])
  end

  private

  def resrtaurants_params
  	params.require(:restaurant).permit(:name, :description)
end

end
