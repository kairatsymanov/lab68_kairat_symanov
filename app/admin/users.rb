ActiveAdmin.register User do

	action_item 'Back', only: [:show, :new, :edit] do
    link_to('Back', :back)
  end

	config.sort_order = 'created_at_asc'
	config.per_page = 10

	filter :name
	filter :email

	permit_params :name, :phone, :address, :admin, :avatar, :created_at, :email, :password, :password_confirmation

	form do |f|
		f.inputs do
			f.input :name
			f.input :email
			f.input :password
			f.input :password_confirmation
			f.input :phone
			f.input :address
			f.input :is_admin
			if f.object.avatar.attached?
				f.input :avatar,
				:as => :file,
				:hint => image_tag(
					url_for(
						f.object.avatar.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
						)
					)
			else
				f.input :avatar, :as => :file
			end
		end
		f.actions
	end

	index do
		selectable_column
		id_column
		column :avatar do |user|
			if user.avatar.attached?
				image_tag user.avatar.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
			end
		end
		column :name do |user|
			link_to user.name, admin_user_path(user)
		end
		column :phone
		column :address
		column :email
		column :admin
		actions
	end

	show do
		attributes_table do
			row :avatar do |user|
				if user.avatar.attached?
					image_tag user.avatar.variant(combine_options: { gravity: 'Center', crop: '50x50+0' })
				end
			end
			row :name
			row :email
			row :password
			row :admin
			row :phone
			row :address
		end
		active_admin_comments
	end

end