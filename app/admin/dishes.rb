ActiveAdmin.register Dish do

	action_item 'Back', only: [:show, :new, :edit] do
		link_to('Back', :back)
	end

	config.sort_order = 'created_at_asc'
	config.per_page = 10

	filter :title
	filter :price
	filter :description

	form do |f|
		f.inputs do
			f.input :title
			f.input :price, as: :number, min: 0, step: :any
			f.input :restaurant_id, label: 'Restaurant', as: :select, 
			collection: Restaurant.all.map{ |resto| [resto.name, resto.id] }
			f.input :description
			if f.object.picture.attached?
				f.input :picture,
				:as => :file,
				:hint => image_tag(
					url_for(
						f.object.picture.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
						)
					)
			else
				f.input :picture, :as => :file
			end
		end
		f.actions
	end

	index do
		selectable_column
		id_column
		column :picture do |dish|
			if dish.picture.attached?
				image_tag dish.picture.variant(
					combine_options: { gravity: 'Center', crop: '50x50+0+0' })
			end
		end
		column :title do |dish|
			link_to dish.title, admin_dish_path(dish)
		end
		column :description
		column :price
		actions
	end

	show do
		attributes_table do
			row :picture do |dish|
				if dish.picture.attached?
					image_tag dish.picture.variant(
						combine_options: { gravity: 'Center', crop: '50x50+0' })
				end
			end
			row :title			
			row :description
			row :price
		end

	end
	permit_params :title, :description, :price, :picture, :created_at, :restaurant_id
end
