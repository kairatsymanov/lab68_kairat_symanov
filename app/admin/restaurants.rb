ActiveAdmin.register Restaurant do
	
	action_item 'Back', only: [:show, :new, :edit] do
		link_to('Back', :back)
	end

	form do |f|
		f.inputs do
			f.input :name
			f.input :description
			if object.image.attached?
				f.input :image,
				:as => :file,
				:hint => image_tag(
					url_for(
						f.object.image.variant(combin_options: { gravity: 'Center', 
							crop: '50x50+0+0' })
						)
					)
			else
				f.input :image, :as => :file
			end
			f.actions
		end
	end

	index do
		selectable_column
		id_column
		column :image do |restaurant|
			if restaurant.image.attached?
				image_tag restaurant.image.variant(
					combine_options: { gravity: 'Center', crop: '50x50+0+0' })
			end
		end
		column :name do |restaurant|
			link_to restaurant.name, admin_restaurant_path(restaurant)
		end
		column :description
		actions
	end

	show do
		attributes_table do
			row :image do |restaurant|
				if restaurant.image.attached?
					image_tag restaurant.image.variant(
						combine_options: { gravity: 'Center', crop: '50x50+0' })
				end
			end
			row :name			
			row :description
		end

	end
	permit_params :name, :description, :image
end
